package com.dragovorn.dnd;

import com.dragovorn.dnd.engine.*;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 03:42.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Core {

    public static final int WIDTH = 500;
    public static final int HEIGHT = 500;

    public final double FRAME_CAP = 5000.0;

    public static final String TITLE = "Dungeons And Dragons";

    private boolean isRunning;

    private Game game;

    public Core() {
        RenderUtil.initGraphics();
        isRunning = false;
        game = new Game();
    }

    public static void main(String[] args) {
        Window.createWindow(WIDTH, HEIGHT, TITLE);

        Core game = new Core();

        game.start();
    }

    public void start() {
        if (isRunning) {
            return;
        }

        run();
    }

    public void stop() {
        if (!isRunning) {
            return;
        }

        isRunning = false;
    }

    private void run() {
        isRunning = true;

        int frames = 0;
        long frameCounter = 0;

        final double updateTime = 1.0 / FRAME_CAP;

        long lastTime = Time.getTime();
        double unprocessedTime = 0;

        while (isRunning) {
            boolean render = false;

            long startTime = Time.getTime();
            long passedTime = startTime - lastTime;
            lastTime = startTime;

            unprocessedTime += passedTime / (double) Time.SECOND;
            frameCounter += passedTime;

            while (unprocessedTime > updateTime) {
                render = true;

                unprocessedTime -= updateTime;


                if (Window.isCloseRequested()) {
                    stop();
                }

                Time.setDelta(updateTime);
                Input.update();

                game.input();
                game.update();

                if (frameCounter >= Time.SECOND) {
                    System.out.println(frames);
                    frames = 0;
                    frameCounter = 0;
                }
            }

            if (render) {
                render();
                frames++;
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException exception) { /* THIS WON'T HAPPEN */ }
            }
        }

        cleanUp();
    }

    private void cleanUp() {
        Window.dispose();
    }

    private void render() {
        RenderUtil.clearScreen();
        game.render();
        Window.render();
    }
}