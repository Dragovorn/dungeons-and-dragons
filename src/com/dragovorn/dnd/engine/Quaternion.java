package com.dragovorn.dnd.engine;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 05:33.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Quaternion {

    private float x;
    private float y;
    private float z;
    private float w;

    public Quaternion(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public float length() {
        return (float) Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
    }

    public Quaternion normalize() {
        float length = length();

        this.x /= length;
        this.y /= length;
        this.z /= length;
        this.w /= length;

        return this;
    }

    public Quaternion conjugate() {
        return new Quaternion(-this.x, -this.y, -this.z, -this.w);
    }

    public Quaternion mul(Quaternion v) {
        float x = this.x * v.getW() + this.w * v.getX() + this.y * v.getZ() - this.z * v.getY();
        float y = this.y * v.getW() + this.w * v.getY() + this.z * v.getX() - this.x * v.getZ();
        float z = this.z * v.getW() + this.w * v.getZ() + this.x * v.getY() - this.y * v.getX();
        float w = this.w * v.getW() - this.x * v.getX() - this.y * v.getY() - this.z * v.getZ();

        return new Quaternion(x, y, z, w);
    }

    public Quaternion mul(Vector3f v) {
        float x = this.w * v.getX() + this.y * v.getZ() - this.z * v.getY();
        float y = this.w * v.getY() + this.z * v.getX() - this.x * v.getZ();
        float z = this.w * v.getZ() + this.x * v.getY() - this.y + v.getX();
        float w = -this.x * v.getX() - this.y * v.getY() - this.z * v.getZ();

        return new Quaternion(x, y, z, w);
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getZ() {
        return this.z;
    }

    public float getW() {
        return this.w;
    }
}