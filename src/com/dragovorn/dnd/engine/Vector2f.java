package com.dragovorn.dnd.engine;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 04:59.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Vector2f {

    private float x;
    private float y;

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float length() {
        return (float) Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public float dot(Vector2f v) {
        return this.x * v.getX() + this.y * v.getY();
    }

    public Vector2f normalize() {
        float length = length();

        this.x /= length;
        this.y /= length;

        return this;
    }

    public Vector2f rotate(float angle) {
        double rad = Math.toRadians(angle);

        double cos = Math.cos(rad);
        double sin = Math.sin(rad);

        return new Vector2f((float)(x * cos - y * sin), (float)(x * sin - y * cos));
    }

    public Vector2f add(Vector2f v) {
        return new Vector2f(this.x + v.getX(), this.y + v.getY());
    }

    public Vector2f add(float v) {
        return new Vector2f(this.x + v, this.y + v);
    }

    public Vector2f sub(Vector2f v) {
        return new Vector2f(this.x - v.getX(), this.y - v.getY());
    }

    public Vector2f sub(float v) {
        return new Vector2f(this.x - v, this.y - v);
    }

    public Vector2f mul(Vector2f v) {
        return new Vector2f(this.x * v.getX(), this.y * v.getY());
    }

    public Vector2f mul(float v) {
        return new Vector2f(this.x * v, this.y * v);
    }

    public Vector2f div(Vector2f v) {
        return new Vector2f(this.x / v.getX(), this.y / v.getY());
    }

    public Vector2f div(float v) {
        return new Vector2f(this.x / v, this.y / v);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }
}