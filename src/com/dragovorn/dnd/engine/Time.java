package com.dragovorn.dnd.engine;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 04:29.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Time {

    public static final long SECOND = 1000000000L; // Becuase we are using nanoseconds

    private static double delta;

    public static long getTime() {
        return System.nanoTime();
    }

    public static double getDelta() {
        return delta;
    }

    public static void setDelta(double delta) {
        Time.delta = delta;
    }
}