package com.dragovorn.dnd.engine;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 04:19.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Window {

    public static void createWindow(int width, int height, String name) {
        Display.setTitle(name);

        try {
            Display.setDisplayMode(new DisplayMode(width, height));

            Display.create();
            Keyboard.create();
            Mouse.create();
        } catch (LWJGLException exception) {
            System.err.println("Error! Could not create window!");
        }
    }

    public static void dispose() {
        Display.destroy();
        Keyboard.destroy();
        Mouse.destroy();
    }

    public static void render() {
        Display.update();
    }

    public static boolean isCloseRequested() {
        return Display.isCloseRequested();
    }

    public static int getWidth() {
        return Display.getDisplayMode().getWidth();
    }

    public static int getHeight() {
        return Display.getDisplayMode().getHeight();
    }

    public static String getTitle() {
        return Display.getTitle();
    }
}