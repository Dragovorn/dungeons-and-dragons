package com.dragovorn.dnd.engine;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.ArrayList;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 04:42.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Input {

    public static final int NUM_KEYCODES = 256;
    public static final int NUM_MOUSEBUTTONS = 18;

    private static ArrayList<Integer> currentKeys = new ArrayList<>();
    private static ArrayList<Integer> downKeys = new ArrayList<>();
    private static ArrayList<Integer> upKeys = new ArrayList<>();

    private static ArrayList<Integer> currentMouse = new ArrayList<>();
    private static ArrayList<Integer> downMouse = new ArrayList<>();
    private static ArrayList<Integer> upMouse = new ArrayList<>();

    public static void update() {
        upMouse.clear();

        for (int x = 0; x < NUM_MOUSEBUTTONS; x++) {
            if (!getMouse(x) && currentMouse.contains(x)) {
                upMouse.add(x);
            }
        }

        downMouse.clear();

        for (int x = 0; x < NUM_MOUSEBUTTONS; x++) {
            if (getMouse(x) && !currentMouse.contains(x)) {
                downMouse.add(x);
            }
        }

        upKeys.clear();

        for (int x = 0; x < NUM_KEYCODES; x++) {
            if (!getKey(x) && currentKeys.contains(x)) {
                upKeys.add(x);
            }
        }

        downKeys.clear();

        for (int x = 0; x < NUM_KEYCODES; x++) {
            if (getKey(x) && !currentKeys.contains(x)) {
                downKeys.add(x);
            }
        }

        currentKeys.clear();

        for (int x = 0; x < NUM_KEYCODES; x++) {
            if (getKey(x)) {
                currentKeys.add(x);
            }
        }

        currentMouse.clear();

        for (int x = 0; x < NUM_MOUSEBUTTONS; x++) {
            if (getMouse(x)) {
                currentMouse.add(x);
            }
        }
    }

    public static boolean getKey(int keyCode) {
        return Keyboard.isKeyDown(keyCode);
    }

    public static boolean getKeyDown(int keyCode) {
        return downKeys.contains(keyCode);
    }

    public static boolean getKeyUp(int keyCode) {
        return upKeys.contains(keyCode);
    }

    public static boolean getMouse(int mouseButton) {
        return Mouse.isButtonDown(mouseButton);
    }

    public static boolean getMouseDown(int mouseButton) {
        return downMouse.contains(mouseButton);
    }

    public static boolean getMouseUp(int mouseButton) {
        return upMouse.contains(mouseButton);
    }

    public static Vector2f getMousePosition() {
        return new Vector2f(Mouse.getX(), Mouse.getY());
    }
}