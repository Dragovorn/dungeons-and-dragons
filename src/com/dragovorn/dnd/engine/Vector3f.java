package com.dragovorn.dnd.engine;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 05:12.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Vector3f {

    private float x;
    private float y;
    private float z;

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float length() {
        return (float) Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public float dot(Vector3f v) {
        return this.x * v.getX() + this.y * v.getY() + this.z * v.getZ();
    }

    public Vector3f normalize() {
        float length = length();

        this.x /= length;
        this.y /= length;
        this.z /= length;

        return this;
    }

    public Vector3f cross(Vector3f v) {
        float x = this.y * v.getZ() - this.z * v.getY();
        float y = this.z * v.getX() - this.x * v.getZ();
        float z = this.x * v.getY() - this.y * v.getX();

        return new Vector3f(x, y, z);
    }

    public Vector3f rotate(float angle) {
        return null;
    }

    public Vector3f add(Vector3f v) {
        return new Vector3f(this.x + v.getX(), this.y + v.getY(), this.z + v.getZ());
    }

    public Vector3f add(float v) {
        return new Vector3f(this.x + v, this.y + v, this.z + v);
    }

    public Vector3f sub(Vector3f v) {
        return new Vector3f(this.x - v.getX(), this.y - v.getY(), this.z - v.getZ());
    }

    public Vector3f sub(float v) {
        return new Vector3f(this.x - v, this.y - v, this.z - v);
    }

    public Vector3f mul(Vector3f v) {
        return new Vector3f(this.x * v.getX(), this.y * v.getY(), this.z * v.getZ());
    }

    public Vector3f mul(float v) {
        return new Vector3f(this.x * v, this.y * v, this.z * v);
    }

    public Vector3f div(Vector3f v) {
        return new Vector3f(this.x / v.getX(), this.y / v.getY(), this.z / v.getZ());
    }

    public Vector3f div(float v) {
        return new Vector3f(this.x / v, this.y / v, this.z / v);
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getZ() {
        return this.z;
    }
}