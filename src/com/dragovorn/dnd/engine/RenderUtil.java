package com.dragovorn.dnd.engine;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 05:51.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class RenderUtil {

    public static void clearScreen() {
        // TODO: STENCIL BUFFER
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public static void initGraphics() {
        glClearColor(1.0f, 0.0f, 2.0f, 0.0f);

        glFrontFace(GL_CW);
        glCullFace(GL_BACK);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);

        // TODO: DEPTH CLAMP FOR LATER

        glEnable(GL_FRAMEBUFFER_SRGB);
    }
}