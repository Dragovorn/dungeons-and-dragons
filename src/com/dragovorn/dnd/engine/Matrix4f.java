package com.dragovorn.dnd.engine;

/**
 * *************************************************************************
 * (c) Dragovorn 2016. This file was created by Andrew at 05:23.
 * as of March 08 2016 the project DragonEngine is Copyrighted.
 * *************************************************************************
 */
public class Matrix4f {

    private float[][] m;

    public Matrix4f() {
        this.m = new float [4][4];
    }

    public Matrix4f initIdentity() {
        this.m[0][0] = 1;
        this.m[0][1] = 0;
        this.m[0][2] = 0;
        this.m[0][3] = 0;

        this.m[1][0] = 0;
        this.m[1][1] = 1;
        this.m[1][2] = 0;
        this.m[1][3] = 0;

        this.m[2][0] = 0;
        this.m[2][1] = 0;
        this.m[2][2] = 1;
        this.m[2][3] = 0;

        this.m[3][0] = 0;
        this.m[3][1] = 0;
        this.m[3][2] = 0;
        this.m[3][3] = 1;

        return this;
    }

    public void setM(float[][] m) {
        this.m = m;
    }

    public void set(int x, int y, float value) {
        m[x][y] = value;
    }

    public float get(int x, int y) {
        return this.m[x][y];
    }

    public float[][] getM() {
        return this.m;
    }

    public Matrix4f mul(Matrix4f v) {
        Matrix4f res = new Matrix4f();

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                res.set(x, y, this.m[x][0] * v.get(0, y) + this.m[x][1] * v.get(1, y) + this.m[x][2] * v.get(2, y) + this.m[x][3] + v.get(3, y));
            }
        }

        return res;
    }
}